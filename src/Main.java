import java.util.InputMismatchException;
import java.util.Scanner;

//Activity S03
public class Main {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int num = 0;
        int ans =1;
        try{
            System.out.println("Input an integer whose factorial will be computed: ");
            num = in.nextInt();
            int neg = num;
            if(num>=0) {
                for (int count = 1; count <= num; count++) {
                    ans = ans * count;

                }
                System.out.println("The factorial of " + num + " is " + ans);
            }else{

                for (int count = 0; count > num; num++) {
                    ans = ans * num;

                }
                System.out.println("The factorial of " + neg + " is " + ans);
            }

        }catch(InputMismatchException e){
            System.out.println("Only numbers are accepted for this system");
//            e.printStackTrace();
        }finally{
            System.out.println("Thank you!!!");
        }
    }
}